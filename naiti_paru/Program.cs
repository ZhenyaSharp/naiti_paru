﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace naiti_paru
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = 0, num2 = 0;
            int[] mas1;
            int[] mas2;
            int[] mas3;
            int[] mas4;
            int countSym = 0;
            bool hasZero;
            const int ZERO_SYMBOL = 0;
            bool parseResult;
            int lvl;
            char answer;
            int step = 0;

            /* do
             {
                 Console.Write("Сколько пар ");
                 parseResult = int.TryParse(Console.ReadLine(), out countSym);
             } while (parseResult == false); */

            do
            {

                do
                {
                    Console.WriteLine("Введите уровень сложности от 1 до 3");
                    lvl = int.Parse(Console.ReadLine());
                } while (lvl > 3 || lvl < 1);

                switch (lvl)
                {
                    case 1:
                        countSym = 5;
                        break;
                    case 2:
                        countSym = 7;
                        break;
                    case 3:
                        countSym = 9;
                        break;
                }

                mas1 = new int[countSym];
                mas2 = new int[countSym];
                mas3 = new int[countSym];
                mas4 = new int[countSym];
                Random rnd = new Random();

                for (int i = 0; i < countSym; i++)
                {
                    mas1[i] = i + 1;
                }

                /* for (int i = 0; i < countSym; i++)
                  {
                      do
                      {
                          Console.Write((i + 1) + " число: ");
                          parseResult = int.TryParse(Console.ReadLine(), out mas1[i]);
                      } while (parseResult == false);
                  } */


                for (int i = 0; i <= countSym - 1; i++) //алгоритм перемешивания от 1 до последнего
                {
                    int j = rnd.Next(0, countSym - 1);
                    int temp = mas1[j];
                    mas1[j] = mas1[i];
                    mas1[i] = temp;
                }

                for (int i = 0; i < countSym; i++)
                {
                    mas2[i] = mas1[i];
                }

                for (int i = countSym - 1; i >= 0; i--) //алгоритм перемешивания в обратном порядке
                {
                    int j = rnd.Next(0, countSym - 1);
                    int temp = mas2[j];
                    mas2[j] = mas2[i];
                    mas2[i] = temp;
                }

                for (int i = 0; i < countSym; i++)
                {
                    Console.Write(mas1[i] + " ");
                }
                Console.WriteLine();

                for (int i = 0; i < countSym; i++)
                {
                    Console.Write(mas2[i] + " ");
                }
                Console.WriteLine("Массивы перемешаны. Игра начинается.");
                Console.ReadKey();

                for (int i = 0; i < countSym; i++)
                {
                    mas3[i] = ZERO_SYMBOL;
                    mas4[i] = ZERO_SYMBOL;
                }

                do
                {
                    hasZero = false;
                    Console.Clear();
                    Console.WriteLine("***ИГРА НАЙДИ ПАРУ***");
                    Console.WriteLine("За нулями спрятаны числа.Найдите все парные числа в первом и втором массивах.");

                    for (int i = 0; i < countSym; i++)
                    {
                        Console.Write(mas3[i] + " ");
                    }
                    Console.WriteLine();
                    for (int i = 0; i < countSym; i++)
                    {
                        Console.Write(mas4[i] + " ");
                    }
                    Console.WriteLine();

                    do
                    {
                        Console.WriteLine("введите номер числа из 1 массива");
                        parseResult = int.TryParse(Console.ReadLine(), out num1);

                    }
                    while (parseResult == false);
                    Console.WriteLine($"это {mas1[num1 - 1]}");

                    do
                    {
                        Console.WriteLine("введите номер числа из 2 массива");
                        parseResult = int.TryParse(Console.ReadLine(), out num2);
                    }
                    while (parseResult == false);
                    Console.WriteLine($"это {mas2[num2 - 1]}");

                    if (mas1[num1 - 1] == mas2[num2 - 1])
                    {
                        Console.WriteLine("Бинго! Вы нашли пару.");
                        mas3[num1 - 1] = mas1[num1 - 1];
                        mas4[num2 - 1] = mas2[num2 - 1];
                        Console.ReadKey();
                    }

                    else
                    {
                        Console.WriteLine("Попробуйте еще раз");
                        Console.ReadKey();
                    }

                    for (int i = 0; i < countSym; i++)
                    {
                        if (mas3[i] == ZERO_SYMBOL)
                        {
                            hasZero = true;
                            break;
                        }
                    }

                    step++;

                } while (hasZero == true);

                Console.Clear();
                Console.WriteLine($"Ура! Победа! Вы прошли игру за {step} ходов. Хотите сыграть еще раз? y/n");
                answer = char.Parse(Console.ReadLine());

            } while (answer == 'y');

            Console.ReadKey();
        }
    }
}
